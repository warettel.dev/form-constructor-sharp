﻿namespace FormConstructor.types;

public struct Name
{
    public readonly string SystemName;
    public string DisplayName;

    public Name(string systemName, string displayName)
    {
        SystemName = systemName;
        DisplayName = displayName;
    }

    public override string ToString()
    {
        return $"System name: {SystemName}\nDisplay name: {DisplayName}";
    }
}