﻿namespace FormConstructor.entities.node_functions;

public abstract class NodeFunction : IExecutable
{
    protected Node ParentNode;

    protected NodeFunction(Node parent)
    {
        ParentNode = parent;
    }

    public virtual void Execute()
    {
    }
}