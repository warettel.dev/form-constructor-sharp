﻿namespace FormConstructor.entities.node_functions;

public interface IExecutable
{
    public void Execute();
}