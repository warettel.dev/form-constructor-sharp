﻿namespace FormConstructor.entities.node_functions;

public class SumFunction : NodeFunction
{
    public SumFunction(Node parent) : base(parent)
    {
    }

    public override void Execute()
    {
        var x = Convert.ToDouble(ParentNode.DataSockets["x"].Data);
        var y = Convert.ToDouble(ParentNode.DataSockets["y"].Data);


        var result = x + y;
        Console.WriteLine($"<{ParentNode.Name.SystemName}> Результат - {x} + {y} = {result}");
        ParentNode.DataSockets["result"].SetData(result);
        ParentNode.ProceedNext(ParentNode.Signals["finish"]);
    }
}