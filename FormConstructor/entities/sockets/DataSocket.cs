﻿using FormConstructor.enums;

namespace FormConstructor.entities.sockets;

public class DataSocket : Socket
{
    public DataSocket(SocketDirection direction, SocketType dataType, Node parentNode) : base(
        direction, dataType, parentNode)
    {
    }

    public object Data { get; private set; }
    public SocketType DataType { get; }

    public void SendData()
    {
        foreach (var socket in ConnectedWith.Cast<DataSocket?>())
            socket?.SetData(Data);
    }

    public void SetData(object data)
    {
        Data = data;
    }
}