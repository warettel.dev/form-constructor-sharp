﻿using FormConstructor.enums;

namespace FormConstructor.entities.sockets;

public abstract class Socket
{
    protected Node ParentNode;

    protected Socket(SocketDirection direction, SocketType socketType, Node parentNode)
    {
        Direction = direction;
        SocketType = socketType;
        ParentNode = parentNode;

        ConnectedWith = new List<Socket>();
        Id = Guid.NewGuid();
    }

    public Guid Id { get; }
    public List<Socket> ConnectedWith { get; }
    public SocketType SocketType { get; }
    public SocketDirection Direction { get; }

    public void Connect(Socket socket)
    {
        if (Direction == socket.Direction || SocketType != socket.SocketType)
            return;

        if (Direction == SocketDirection.Inbound)
        {
            if (socket.ConnectedWith.Contains(this))
                return;

            socket.ConnectedWith.Add(this);
        }
        else
        {
            if (ConnectedWith.Contains(socket))
                return;

            ConnectedWith.Add(socket);
        }
    }
}