﻿using FormConstructor.entities.node_functions;
using FormConstructor.enums;

namespace FormConstructor.entities.sockets;

public class SignalSocket : Socket
{
    public SignalSocket(SocketDirection direction, Node parentNode,
        NodeFunction? function = null) : base(direction,
        SocketType.Signal, parentNode)
    {
        if (direction == SocketDirection.Inbound && function == null)
            throw new Exception("Socket function is null");

        NodeFunction = function;
    }

    public NodeFunction NodeFunction { get; }

    public void Activate()
    {
        if (Direction == SocketDirection.Inbound)
            ParentNode.Activate(NodeFunction);
    }

    public void GoForward()
    {
        foreach (var signalSocket in ConnectedWith.Cast<SignalSocket?>())
            signalSocket?.Activate();

        //DEBUG
        if (ConnectedWith.Count == 0)
            Console.WriteLine("Конец цепочки");
    }
}