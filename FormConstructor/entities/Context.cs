﻿using FormConstructor.types;

namespace FormConstructor.entities;

public class Context
{
    private readonly List<Node> _scopedNodes;

    public Context(Name name)
    {
        Name = name;
        _scopedNodes = new List<Node>();
    }

    public Name Name { get; }

    public void AddToContext(Node node)
    {
        _scopedNodes.Add(node);
    }

    public void RemoveFromContext(Node node)
    {
        _scopedNodes.Remove(node);
    }
}