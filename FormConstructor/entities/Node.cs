﻿using FormConstructor.entities.node_functions;
using FormConstructor.entities.sockets;
using FormConstructor.enums;
using FormConstructor.types;

namespace FormConstructor.entities;

public class Node
{
    public Node(Context context, Name name)
    {
        Context = context;
        Name = name;
        Signals = new Dictionary<string, SignalSocket>();
        DataSockets = new Dictionary<string, DataSocket>();
        Functions = new Dictionary<string, NodeFunction>();
        Id = Guid.NewGuid();
    }

    public Guid Id { get; }
    public Context Context { get; }
    public Name Name { get; }
    public Dictionary<string, SignalSocket> Signals { get; }
    public Dictionary<string, DataSocket> DataSockets { get; }
    public Dictionary<string, NodeFunction> Functions { get; }

    //API для активации текущей ноды
    public void Activate(NodeFunction function)
    {
        foreach (var socket in DataSockets.Values.Where(socket => socket.Direction == SocketDirection.Inbound))
            if (socket.Data == null)
                return;

        function.Execute();
    }

    //API для активации следующих нод
    public void ProceedNext(SignalSocket signal)
    {
        foreach (var dataSocket in DataSockets)
            if (dataSocket.Value.Direction == SocketDirection.Outbound)
                dataSocket.Value.SendData();

        signal.GoForward();
    }

    // API для взаимодействия с функциями
    public Node AddFunction(string name, NodeFunction function)
    {
        Functions.Add(name, function);
        return this;
    }

    // API для взаимодействия с сигналами
    public Node AddSignal(string name, SignalSocket signal)
    {
        Signals.Add(name, signal);
        return this;
    }

    public Node RemoveSignal(string name)
    {
        Signals.Remove(name);
        return this;
    }

    public Node ClearSignals()
    {
        Signals.Clear();
        return this;
    }

    // API для взаимодействия с сокетами
    public Node AddSocket(string name, DataSocket socket)
    {
        DataSockets.Add(name, socket);
        return this;
    }

    public Node RemoveSocket(string name)
    {
        DataSockets.Remove(name);
        return this;
    }

    public Node ClearSockets()
    {
        DataSockets.Clear();
        return this;
    }

    public override string ToString()
    {
        const string separator = "--------------------";
        var content = $"Node ID: {Id}\n{Name}\nSignals: {Signals.Count}\nSockets: {DataSockets.Count}";

        return $"{separator}\n{content}\n{separator}";
    }
}