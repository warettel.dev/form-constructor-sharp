﻿using FormConstructor.entities.node_functions;
using FormConstructor.entities.sockets;
using FormConstructor.enums;
using FormConstructor.types;

namespace FormConstructor.entities.node_factories;

public class SumNodeFactory : INodeFactory
{
    public Node CreateNode(Context ctx)
    {
        if (ctx == null) throw new Exception("Missing context!");

        // Объявление ноды
        var node = new Node(ctx, new Name("sum_node", "Sum Node"));

        // Объявление функции
        var sumFunction = new SumFunction(node);

        //Объявление сигналов
        var inSignal = new SignalSocket(SocketDirection.Inbound, node, sumFunction);
        var outSignal = new SignalSocket(SocketDirection.Outbound, node);

        //Объявление сокетов
        var xSocket =
            new DataSocket(SocketDirection.Inbound, SocketType.Number, node);
        var ySocket =
            new DataSocket(SocketDirection.Inbound, SocketType.Number, node);
        var resultSocket =
            new DataSocket(SocketDirection.Outbound, SocketType.Number, node);

        //Добавление функций, сигналов и сокетов в ноду
        node.AddFunction("sum", sumFunction)
            .AddSignal("start", inSignal)
            .AddSignal("finish", outSignal)
            .AddSocket("x", xSocket)
            .AddSocket("y", ySocket)
            .AddSocket("result", resultSocket);

        return node;
    }
}