﻿namespace FormConstructor.entities.node_factories;

public interface INodeFactory
{
    public Node CreateNode(Context? context);
}