﻿// See https://aka.ms/new-console-template for more information

using FormConstructor.entities;
using FormConstructor.entities.node_factories;
using FormConstructor.types;

var ctx = new Context(new Name("appContext", "Application Context"));

var factory = new SumNodeFactory();

var sumNode = factory.CreateNode(ctx);
var fourNode = factory.CreateNode(ctx);
var nextSumNode = factory.CreateNode(ctx);
var anotherSumNode = factory.CreateNode(ctx);

//Default start values
sumNode.DataSockets["x"].SetData(5);
sumNode.DataSockets["y"].SetData(20);
fourNode.DataSockets["x"].SetData(200);

//Connect edges
sumNode.Signals["finish"].Connect(nextSumNode.Signals["start"]);
sumNode.DataSockets["result"].Connect(nextSumNode.DataSockets["x"]);
sumNode.DataSockets["result"].Connect(nextSumNode.DataSockets["y"]);

sumNode.Signals["finish"].Connect(fourNode.Signals["start"]);
sumNode.DataSockets["result"].Connect(fourNode.DataSockets["y"]);

fourNode.Signals["finish"].Connect(anotherSumNode.Signals["start"]);
fourNode.DataSockets["result"].Connect(anotherSumNode.DataSockets["x"]);

nextSumNode.Signals["finish"].Connect(anotherSumNode.Signals["start"]);
nextSumNode.DataSockets["result"].Connect(anotherSumNode.DataSockets["y"]);

sumNode.Signals["start"].Activate();