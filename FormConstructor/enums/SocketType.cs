﻿namespace FormConstructor.enums;

public enum SocketType
{
    Signal,
    Boolean,
    Number,
    String,
    Json
}