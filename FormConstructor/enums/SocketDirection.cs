﻿namespace FormConstructor.enums;

public enum SocketDirection
{
    Inbound,
    Outbound
}